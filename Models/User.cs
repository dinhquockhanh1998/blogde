﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace BlogDE.Models
{
    public class User
    {
        public User()
        {
           
            Posts = new HashSet<Post>();
        }
        public virtual int Id { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public int RoleID { get; set; }
       
        public virtual Role Role { get; set; }
        public virtual ICollection<Post> Posts { get; set; }
    }
}
