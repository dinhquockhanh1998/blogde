﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace BlogDE.Models
{
    public class Tag
    {
        public Tag()
        {
            PostTags = new HashSet<PostTag>();
        }
        public int TagId { get; set; }

        public string Title { get; set; }
        public virtual ICollection<PostTag> PostTags { get; set; }
    }
}
