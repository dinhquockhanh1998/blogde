﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlogDE.Models
{
    public class Role
    {
        public Role()
        {
            Users = new HashSet<User>();
        }
        public virtual int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public ICollection<User> Users { get; set; }

    }
}
