﻿using BlogDE.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlogDE.Data
{
    public class PostDAO
    {
        private BlogDbContext db;
        public PostDAO(BlogDbContext context)
        {
            this.db = context;
        }

        public bool AddPost(Post post)
        {
            try
            {
                db.Posts.Add(post);
                db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        // assume that the 'Post' object is previously retrived from database via PostContext
        public bool UpdatePost(Post post)
        {
            try
            {
                db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeletePost(Post Post)
        {
            try
            {
                db.Posts.Remove(Post);
                db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public IEnumerable<Post> GetAllPosts()
        {
            return db.Posts; // this is lazy loading. The data is only loaded when we loop over it
        }

        // get all Posts by cpu name
        public List<Post> GetPostByAddedDate(Post AddedDate)
        {
            return db.Posts.Where(l => l.AddedDate.Equals(AddedDate)).ToList<Post>();
        }

        public int GetPostCount()
        {
            return (from Post in db.Posts
                    select Post).Count();
        }
    }
}
