﻿using BlogDE.Controllers;
using BlogDE.Models;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;

namespace BlogDE.Data
{
    public static class DbInitializer
    {
        public static void Initialize(BlogDbContext context)
        {
            context.Database.EnsureCreated();

            //// Look for category if existing then return
            if (context.Categories.Any())
            {
                return;
            }

            var categories = new Category[] {
                new Category{ Name=".Net Core" },
                new Category{ Name="EF Core" },
            };

            context.Categories.AddRange(categories);
            context.SaveChanges();


            var posts = new Post[] {
                new Post{ CategoryId=1,Title="Get started with .Net Core",FullDescription="Create new .Net Core app" },
                new Post{ CategoryId=2,Title="Get started with EF Core",FullDescription="Apply Ef core code first" },
            };

            context.Posts.AddRange(posts);
            context.SaveChanges();

               var roles = new Role[] {
                new Role{ Name="User" },
                new Role{ Name="Admin" },
            };

            context.Roles.AddRange(roles);
            context.SaveChanges();

            if (context.Users.Any())
            {
                return;
            }
            
            //PasswordHasher<string> passwordHasher = new PasswordHasher<string>();
         
            var users = new User[] {
                new User{RoleID=1,Username="user",Password= GetMD5Hash("user"),Email="user@gmail.com",FirstName="John", LastName="Smith" },
                new User{RoleID=2,Username="admin",Password= GetMD5Hash("admin"),Email="admin@gmail.com",FirstName="Ryan", LastName="Nguyen" },
            };

            context.Users.AddRange(users);
            context.SaveChanges();

           
        }
        public static string GetMD5Hash(string input)
        {
            using (MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider())
            {
                byte[] b = System.Text.Encoding.UTF8.GetBytes(input);
                b = md5.ComputeHash(b);
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                foreach (byte x in b)
                {
                    sb.Append(x.ToString("x2"));
                }
                return sb.ToString();
            }

        }
    }
}
