﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BlogDE.Data;
using BlogDE.Models;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

namespace BlogDE.Controllers
{

    [Authorize]
    public class PostController : Controller
    {
        //private PostDAO postDAO = new PostDAO(new BlogDbContext());
        private BlogDbContext db;
        public PostController(BlogDbContext context)
        {
            this.db = context;
        }
        [HttpGet]
        public async Task<IActionResult> Index(
            string sortOrder,
            string currentFilter,
            string searchString,
            int? pageNumber)
        {

            ViewData["CurrentSort"] = sortOrder;
            ViewData["TitleSortParm"] = String.IsNullOrEmpty(sortOrder) ? "title_desc" : "";
            ViewData["DateSortParm"] = sortOrder == "Date" ? "date_desc" : "Date";
            ViewData["IdSortParm"] = sortOrder == "Id" ? "Id_desc" : "Id";

            if (searchString != null)
            {
                pageNumber = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewData["CurrentFilter"] = searchString;

            var posts = from s in db.Posts.Include(p => p.Category)
                                           .Include(p => p.User)
                        select s;
            if (!String.IsNullOrEmpty(searchString))
            {
                posts = posts.Where(s => s.Title.Contains(searchString)
                                       || s.ShortDescription.Contains(searchString));
            }

            switch (sortOrder)
            {
                case "title_desc":
                    posts = posts.OrderByDescending(s => s.Title);
                    break;
                case "Date":
                    posts = posts.OrderBy(s => s.AddedDate);
                    break;
                case "date_desc":
                    posts = posts.OrderByDescending(s => s.AddedDate);
                    break;
                case "Id":
                    posts = posts.OrderBy(s => s.Id);
                    break;
                case "Id_desc":
                    posts = posts.OrderByDescending(s => s.Id);
                    break;
                default:
                    posts = posts.OrderBy(s => s.Title);
                    break;
            }
            int pageSize = 3;
            return View(await PaginatedList<Post>.CreateAsync(posts.AsNoTracking(), pageNumber ?? 1, pageSize));
        }

        [HttpGet]
        public ActionResult Create()
        {
            ViewData["CategoryId"] = new SelectList(db.Categories, "CategoryId", "Name");
            return View();
        }
        [HttpPost]
        public ActionResult Create(Post post)
        {
            if (ModelState.IsValid)
            {
                post.AddedBy = Request.Cookies["Name"];
                post.AddedDate = DateTime.Now;
                db.Posts.Add(post);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewData["CategoryId"] = new SelectList(db.Categories, "CategoryId", "Name", post.CategoryId);
            return View(post);


        }

        [HttpGet]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var post = db.Posts.Find(id);
            
            if (post == null)
            {
                return NotFound();
            }
            ViewData["CategoryId"] = new SelectList(db.Categories, "CategoryId", "Name", post.CategoryId);
            return View(post);
        }

        [HttpPost]
        public ActionResult Edit(int id, Post post)
        {
            if (id != post.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    Post rel = db.Posts.Find(post.Id);
                    post.AddedDate = rel.AddedDate;
                    post.AddedBy = rel.AddedBy;
                    post.ModifiedBy = Request.Cookies["Name"];
                    post.ModifiedDate = DateTime.Now;
                    db.Entry(rel).CurrentValues.SetValues(post);
                    db.SaveChanges();
                    
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PostsExists(post.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            ViewData["CategoryId"] = new SelectList(db.Categories, "CategoryId", "Name", post.CategoryId);
            return View(post);
        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var post = db.Posts
                .Include(p => p.Category)
                .FirstOrDefault(m => m.Id == id);
            if (post == null)
            {
                return NotFound();
            }

            return View(post);
        }

        [HttpPost]
        public ActionResult Delete(int id, Post post)
        {
            if (id != post.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    db.Remove(post);
                    db.SaveChanges();

                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PostsExists(post.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            return View(post);
        }

        private bool PostsExists(int id)
        {
            return db.Posts.Any(e => e.Id == id);
        }

    }
}