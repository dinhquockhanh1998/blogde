﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Threading.Tasks;
using BlogDE.Data;
using BlogDE.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

namespace BlogDE.Controllers
{
    public class AccountController : Controller
    {
        private BlogDbContext db;

        public AccountController(BlogDbContext context)
        {
            this.db = context;
        }

      

        [HttpGet]
        public IActionResult Login(string returnUrl =null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            if (Request.Cookies[".AspNetCore.Cookies"] != null)
                return RedirectToAction("Index", "Home");
            return View();
        }

        [HttpPost]
        public ActionResult Login(User user, string returnUrl =null)
        {

            var _account =  db.Users.Where(s => s.Username == user.Username && s.Password == GetMD5Hash(user.Password)).Include(p => p.Role);
            var account = _account.FirstOrDefault();

            if (_account.Any())
            {
               
                //Check roles and sign authentication
                CookieOptions cookie = new CookieOptions();
                cookie.IsEssential = true;
                Response.Cookies.Append("UserName", account.Username, cookie);
                Response.Cookies.Append("Name", account.FirstName, cookie);

                var identity = new ClaimsIdentity(new[]{
                        new Claim(ClaimTypes.Role,account.Role.Name)}, CookieAuthenticationDefaults.AuthenticationScheme);
                var principal = new ClaimsPrincipal(identity);
                HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, principal);

                return RedirectToLocal(returnUrl);
                // If Role has go to .....
                //End check roles and authentication

            }


            return View();
        }
        [HttpGet]
        public ActionResult Register()
        {
            ViewData["RoleID"] = new SelectList(db.Roles, "Id", "Name");
            return View();
        }

        // POST: Admin/Accounts/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        public ActionResult Register(User user)
        {
            if (ModelState.IsValid)
            {
                user.Password = GetMD5Hash(user.Password);
                db.Users.Add(user);
                db.SaveChanges();
                return View("Login", user);
            }
            ViewData["RoleID"] = new SelectList(db.Roles, "Id", "Name", user.RoleID);
            return View("Login",user);
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

        public IActionResult Logout()
        {
            HttpContext.SignOutAsync();
            Response.Cookies.Delete("UserName");
            return RedirectToAction("Index", "Home");

        }

        //Hash password.......
        public static string GetMD5Hash(string input)
        {
            using (MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider())
            {
                byte[] b = System.Text.Encoding.UTF8.GetBytes(input);
                b = md5.ComputeHash(b);
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                foreach (byte x in b)
                {
                    sb.Append(x.ToString("x2"));
                }
                return sb.ToString();
            }

        }


    }
}