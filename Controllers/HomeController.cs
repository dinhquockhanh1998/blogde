﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using BlogDE.Models;
using Microsoft.EntityFrameworkCore;
using BlogDE.Data;

namespace BlogDE.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly BlogDbContext db;

        public HomeController(ILogger<HomeController> logger,BlogDbContext context)
        {
            _logger = logger;
            this.db = context;
        }
      
        public async Task<IActionResult> Index(string sortOrder,
              int? pageNumber, string searchString, string currentFilter)
        {
            if (Request.Cookies[".AspNetCore.Cookies"] != null)
                ViewBag.Name = Request.Cookies["UserName"];
            ViewData["CurrentSort"] = sortOrder;
            ViewData["CurrentFilter"] = searchString;

            int pageSize = 3;
            var blog = from s in db.Posts
                       select s;
            blog = blog.Where(s => s.Published == true);

         

            if (searchString != null)
            {
                pageNumber = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            blog = blog.OrderByDescending(o => o.AddedDate);
            //var temp1 = blog.Where(o => o.Name.Contains(a)).ToList();
            if (!String.IsNullOrEmpty(searchString))
            {
                blog = blog.Where(s => s.ShortDescription.Contains(searchString) || s.Title.Contains(searchString) && s.Published == true);
            }

            return View(await PaginatedList<Post>.CreateAsync(blog.AsNoTracking(), pageNumber ?? 1, pageSize));
        }
        
        public async Task<ActionResult> DetailView(int? id)
        {
            if (Request.Cookies[".AspNetCore.Cookies"] != null)
            {
                ViewBag.Name = Request.Cookies["UserName"];
            }
            if (id == null)
            {
                return NotFound();
            }
           
            var posts = await db.Posts
                .FirstOrDefaultAsync(m => m.Id == id);
            if (posts == null)
            {
                return NotFound();
            }

            return View(posts);
        }


        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
